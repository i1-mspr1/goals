# EPSI Project : I1 MSPR Cerealis
## Project authors
| Lastname  | Firstname |
|-----------|-----------|
| DUHEM     | Matthieu  |
| RAOUL     | Antoine   |
| LECAPLAIN | Adrien    |
| SAPIN     | Nicolas   |
| JAHAN     | Robin     |

# EN
## Oral :
	- 20 min oral in English + demo included
	- 10 min of questions

## Questions : 
	- Collecting user needs? *standby*
	- Leading the change with the business during the deployment of an application or integrated solution by implementing a participation, communication and training approach to support users in integrating the new tool into their working habits.
	to help users integrate the new tool into their work habits. => Tuto? Installation guide, README
	- *Prospect users? CRM/ERP find an open source ERP/CRM containing an API that will be called to insert leads into the CRM
	- Other support like tablets? If possible and not too complex => mobile app sufficient
	- If a box is not coloured, display the part not drawn in white

## Objectives:
	- Android / iOS application (optional) on public stores (optional)
	- CI/CD (tests + build + APK)
	- Management method (agile => SCRUM)
	- Prospecting users? CRM/ERP
	- Retrieving user information by email

## Company Information
    - Company name
    - Name: Cerealis 
    - Founded: 1932
    - Type: Food Food industry
    - Production : 
    	- Quiktos
    	- Alpen
    	- Crispy
    -Location:
    	- England => Northamptonshire
    	- Canada => Cobourg
    -Added value => use of VR (virtual reality) and AR (augmented reality)

## Product :
	Targets :
		- Young audience 6 to 12 years old 
	Features:
		- QR Code on the package to install the app
		- Launch the app and view the drawing via the camera and see the animation of the drawing with the right colours
		- Language of the application English only
		- Integrate the 3 drawings provided by the company (accessible offline)
		- Model the 3d models (not necessarily realistic)
		- Animate or not the models
		- Popup with first name + email to share a screenshot on social networks with a particular hashtag (#cerealis #coloring #AR)

## Existing archicteture:
	- Website managed & maintained by an external company
	- Social media presence managed by the same external service provider involving a community manager

## Technical stack:
	Project management: SCRUM + Board gitlab
	Versioning: Gitlab
	ERP/CRM: 
		- Odoo
		- Bdd to be implemented in the tool + creation of schema and scripts (useful for e2e)
	Mobile application: 
			Type of application:
				- PWA
			Development approach:
				- Mobile first
			Native features:
				- *Camera?
				- Screen capture?
			Framework:
				- VueJS -> TS (Warning) => *Implementation of TS incomplete or complex
			Cross OS 
				- Yes
	3d modelling:
		- Blender
	Image recognition:
		- AR.js
	3d representation:
		- AR.js
		- Three.JS
    *Point to be determined or to be deepened

# Rules to respect
## GIT:
### Branch naming:
Each time a branch is created from develop or a feature branch from develop, it must be preceded by two possible ways:

 	- feat/feature-name for features
 	- fix/bug-name for bugs
BE CAREFUL with "-"!

### Merge request
When making a merge request you must make sure that there is:
	- An assignee
	- A reviewer
	- A success from the various pipelines
	- An approved merge request
	- Branch feat/ or fix/ to the develop branch

## Communication:

If there are problems, you need to communicate with the people in the group to avoid the story taking longer or being seen as pretending to work.
A chosen story is a story to be respected within its deadlines unless it is really blocking.
If you don't respect the rules, you are subject to various remarks that are not necessarily pleasant and on top of that, you penalize the group. Be aware of your actions and be responsible. Let's hear it!

# FR

## Oral :
	- 20 min d'oral en anglais + demo incluse
	- 10 min de questions

## Questions : 
	- Collecter les besoins utilisateurs? *standby
	- Conduire le changement auprès des métiers lors du déploiement d’une solution applicative ou intégrée en
	mettant en place une démarche de participation, de communication et de formation pour accompagner les
	utilisateurs à l’intégration du nouvel outil dans leurs habitudes de travail. => Tuto? Guide d'installation, README
	- *Prospecter les users? CRM/ERP trouver un ERP/CRM open source contenant une API qui sera appelée pour insérer les prospects dans le CRM
	- Autre support type tablettes? Si c'est possible et pas trop complexe => appli mobile suffisante
	- Si une case est non coloriée, affichage de la partie non dessinée en blanc

## Objectifs:
	- Application Android / iOS (optionnel?) sur les stores publiques(optionnel)
	- CI/CD (tests + build + APK)
	- Méthode managériale (agile => SCRUM)
	- Prospecter les users? CRM/ERP
	- Récupération des informations utilisateurs par email

## Entreprise:
	Nom : Cerealis 
	Création : 1932
	Type : Agroalimentaire
	Production : 
		- Quiktos
		- Alpen
		- Crispy
	Localisation:
		- Angleterre => Northamptonshire
		- Canada => Cobourg

	Plus value => utilisation de la VR (réalité virtuelle) et de l'AR(réalité augmenté)
	
	Produit :
		Cibles :
			- Jeune public 6 à 12 ans 
		Features:
			- QR Code sur le paquet pour installer l'appli
			- Lancer l'application et visualiser le dessin via la caméra et voir l'animation du dessin avec les bonnes couleurs
			- Langue de l'application anglais uniquement
			- Intégrer les 3 dessins fournis par l'entreprise (accessibles hors connexion)
			- Modéliser les modèles 3d (pas forcément réaliste)
			- Animer ou non les modèles
			- Popup avec prénom + email pour partager une copie d'écran sur les réseaux sociaux avec un hashtag particulier (#cerealis #coloring #AR)

## Archicteture existante :
	- Site web gérée & maintenu par une société externe
	- Présence sur les réseaux sociaux gérée par le même prestataire externe faisant intervenir un community manager

## Stack technique:
	Gestion de projet: SCRUM + Board gitlab
	Versionning: Gitlab
	ERP/CRM: 
		- Odoo
		- Bdd à implémenter dans l'outil + création du schéma et des scripts (utile pour l'e2e)
	Application mobile: 
			Type d'application:
				- PWA
			Approche de développement:
				- Mobile first
			Fonctions natives:
				- *Caméra?
				- *Capture d'écran?
			Framework:
				- VueJS -> TS (Warning) => *Implémentation de TS imcomplète voire complexe
			Cross OS 
				- Oui
	Modélisation 3d:
		- Blender
	Reconnaissance d'image:
		- AR.js
	Représentation 3d:
		- AR.js
		- Three.JS
    *Point à déterminer ou à approfondir

# Règles à respecter
## GIT:
### Nomination des branches:
À chaque création de branche doit partir de develop ou d'une branche de feature issue de develop, celle-ci doit être précédée de deux façons possible:

 	- feat/nom-de-la-feature pour les fonctionnalités
 	- fix/nom-du-bug pour les bugs
ATTENTION aux "-"!

### Merge request
Lors d'une merge request vous devez vous assurez qu'il y est:
	- Un assigné
	- Un examinateur
	- Un succès des divers pipelines
	- Une merge request approuvée
	- Branche feat/ ou fix/ vers la branche develop

## Communication:

En cas de problèmes, il faut communiquer avec les personnes du groupe pour pouvoir éviter que la story prennent plus de temps ou que l'on croit que vous faites semblant de bosser.
Une story choisie est une story à respecter dans ses délais sauf si elle est vraiment bloquante.
En cas de non respect des règles, vous êtes sujet à diverses remarques pas forcément agréables et en plus de ça, vous pénalisez le groupe. Ayez conscience de vos actes et soyez responsable. À bon entendeur !
